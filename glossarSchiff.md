---
title: Glossar Schiff
author:
  - Oliver Nakoinz
#date: "Version 2  <br>  März 2025 <br>  [![License: CC BY-SA 4.0](./public/License-CCBYSA4.svg)](https://creativecommons.org/licenses/by-sa/4.0/)"
date: "Version 2  <br>  März 2025 <br>  [![License: CC BY-SA 4.0](License-CCBYSA4.svg)](https://creativecommons.org/licenses/by-sa/4.0/)"
#date: "Version 2  <br>  März 2025 <br>  [![License: CC BY-SA 4.0](License-CCBYSA4.pdf)](https://creativecommons.org/licenses/by-sa/4.0/)"
papersize: A4
fontsize: 12pt
#font-family: 'Garamond' 
font-family: 'FreeSerif'
#font-family: 'Helvetica'
documentclass: scrartcl
margin-left: 2.0cm
margin-right: 2.0cm
margin-top: 2.0cm
margin-bottom: 3.0cm
output:
  html_document:
    toc: yes
    toc_depth: 5
    toc_float: yes
    pandoc_args:
        - --css
        - style.css
  pdf_document:
    toc: true
    toc_depth: 5
header-includes:
  \usepackage{placeins}
lang: de-DE
otherlangs: en-GB
self_contained: yes
pandoc_args: [ 
      "--filter", "pandoc-quotes" 
    ]
Comment: xxxxxxxxx
---


<!--
**Code:**      
path="/home/fon/.pim/wiki/glossare/glossarschiff/"
file="glossarSchiff"
cd $path

pandoc -s -f markdown -t html  $file.md -o "./public/index.html"
cp /public/index.html "/home/fon/pCloudDrive/Public Folder/Glossar/GlossarSchiff.html"
pandoc -s -f markdown -t latex $file.md -o $file.pdf --pdf-engine=xelatex

cd /home/fon/pCloudDrive/info/glossarschiff
git pull
git add .
git commit -m "Ergänzungen und Korrekturen"
git push


**Info:**

- http://pandoc.org/MANUAL.html#variables-set-by-p
- https://github.com/citation-style-language/styles

Footnotes[^1] xxxxxxxxx

[^1]: 
Footnotes

**Notizen:**
xxxxxxx
-->

# Inhalt

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e)  [F](#f)  [G](#g)  [H](#h) [I](#i) [J](#j) [K](#k) 
[L](#l) [M](#m) [N](#n) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) 
[W](#w) [X](#x) [Y](#y) [Z](#z) 
[Quellen](#quellen)

<!-- \twocolumn  ############################## für html raus #############-->


## A

[<Inhalt](#inhalt)

**[Achter-, achtern]{#achter}**
:    abaft, after, aft (engl.), agterfor (dk.)
:    Indiziert "hinten" aus Sicht der Fahrtrichtung

*[Achterdeck]{#Achterdeck}*  
:    stern, afterbody (engl.), agterdæk (dk.)     
:    Achterdeck

**Achtersteven**  
:    [→Steven](#Achtersteven)

*[Achterschiff]{#Achterschiff}*  
:    stern, afterbody (engl.), agterskip (dk.)     
:    Hinterer Teil des Schiffes

*[achteraus]{#achteraus}*
:    astern (engl.), agterude (dk.)
:    Indiziert einen Punkt in Fahrrichtung hinter dem Schiff

**[Achterliek]{#Achterliek}**  
:    Kante eines [→Stagsegels](#Stagsegel), die vom [→Mast](#Mast) wegweist

**[Anker]{#Anker}**  
:    anchor (engl.), anker (dk.)     
:    Gerät, um das Fahrzeug am Meeresboden zu verhaken

**[Ahmings]{#Ahmings}**  
:    Tiefgangsmarken

**[Aufgeien]{#Aufgeien}**  
:    Heranziehen eines Segels an die [→Rah](#Rah)

**[Aufkimmung]{#Aufkimmung}**
:    deadrise (engl.), bundrejsning (dk.)
:    Winkel zwischen Horizontale und Boden an der [→Kimm](#Kimm)

**[Auflanger]{#Auflanger}**  
:    futtock (engl.),  oplænger (dk.)
:    Der Teil des [→Spant](#Spant), der die Bordwand abdeckt und die Bodenwrange an der Bordwand fortsetzt

**[aufschießen]{#aufschießen}**  
:    Das [Tauwerk](#Tauwerk) geordnet hinlegen

**[Auge]{#Auge}**  
:    Runde oder ovale Öse


## B

[<Inhalt](#inhalt)

**[Back]{#Back}**   
:    Deckhaus auf dem Vorschiff, das sich über die volle Rumpfbreite ausdehnt
 
**[Backbord]{#Backbord}**  
:    portside, port (engl.), bagbord (dk.)    
:    In Fahrtrichtung linke Schiffsseite  

**[backbrassen]{#backbrassen}**  
:    Die [→Rahsegel](#Rahsegel) so orientieren, dass der Wind von vorn in die Segel drückt

**[Balken]{#Balken}**  
:    beam  (engl.) , bjælke (dk.)
:    massives, längliches, oft horizontal angebrachtes Holz

**[durchgehender Balken]{#dBalken}**  
:    protruding beam, throughbeam  (engl.), udkraget bjælke (dk.)
:    [→Balken](#Balken), der die Bordwand durchstößt

**[Balkenbucht]{#Balkenbucht}**  
:    camber  (engl.), bugt, bjælkebugt (dk.)  
:    quer verlaufende Wölbung eines Decks zwecks Wasserablauf

**[Balkenstütze]{#Balkenstütze}**  
:    stanchion (engl.)
:    Senkrechtes Holz, das einen Balken stützt

**[Balkweger]{#Balkweger}**  
:    shelf clamp (engl.), bjælkevæger, bitevæger (dk.)
:    [→Klampe](#Klampe), die als Unterlage für Balkenenden verwendet wird

**[Baum]{#Baum}** 
:    Stange, die am unteren Teil des [→Mastes](#Mast) an einem Ende horizontal und drehbar angebracht ist

**[Baumnock]{#Baumnock}**  
:    Das hintere Ende des [→[Baums](#Baum)

**[belegen]{#belegen}**  
:     Kreuzweises Befestigen von [→Tauen](#Tauwerk) an [→Pollern](#Poller), [→Klampen](#Klampe) etc.

**[Belegnagel]{#Belegnagel}**  
:    Herausnehmbare kurze Holzstange, die zur Befestigung von [→Tauen](#Tauwerk) an [→Balken](#Balken) dient und die in ein Loch dieses Balkens gesteckt werden

**[Besanmast]{#Besanmast}**  
:    Hinterer [→Mast](#Mast) eines Schiffs

**[Besansegel]{#Besansegel}**  
:    Unteres [→Segel](#Segel) am [→Besanmast](#Besanmast)

**[Bilge]{#Bilge}**
:    bilge (engl.), bund, flak (dk.)
:    Unterster Bereich eines Schiffes, der direkt oberhalb des Schiffsbodens bzw. des [→Kiels](#Kiel) liegt 

**[Bite]{#Bite}**
:    biti, bite, lower crossbeam (engl.), bite (dk.)
:    unterster Querbalken, wenn mehrere übereinander angeordnete Ebenen von Querbalken vorliegen, der auf der [→Bodenwrange](#Bodenwrange) unverbunden aufliegt und gewissermaßen Module des [→Spant](#Spant) trennt, typisch für den Nordischen Schiffbau

**[Block]{#Block}**  
:    block, pulley-block (engl.), blok , taljeblok (dk.)
:    Gehäuse mit einer oder mehreren drehbaren Seilscheiben zum Umlenken von [→Tauwerk](#Tauwerk)

*[scheibenloser Block]{#sBlock}*
:    fairlead (engl.), skivløs blok (dk.)
:    Block ohne drehbare Seilscheiben

*[Blockscheibe]{#Blockscheibe}*, 
:    blokskive (dk.)
:    Scheibe in einem Block

**[Boden]{#Boden}**  
:    bilge, bottom, floor (engl.), bund (dk.)
:    Unterer Bereich des Schiffsrumpfes

**[Bodenwrange, Lieger]{#Bodenwrange}**  
:    floor-timber (engl.), bundstok (dk.)    
:    Teil des [→Spants](#Spant), der den Schiffsboden abdeckt und unter der Bordwand oder in deren unterem Bereich endet und ggf. durch [→Auflanger](#Auflanger) ergänzt wird 

**[Bodenbauweise]{#Bodenbauweise}**  
:    bottom-based (engl.), bygget på plankebund (dk.)
:    Bauweise, bei der zuerst die Bodenplanken verbunden und ausgelegt werden, bevor mit dem Spantenskelett, insbesondere den [→Bodenwrangen](#Bodenwrange) begonnen wird. Diese Bauweise steht im Gegensatz zur [→Skelettbauweise](#Skelettbauweise) und ist entweder als Variante der [→Schalenbauweise](#Schalenbauweise) oder als Mischform dieser mit der [→Skelettbauweise](#Skelettbauweise) (wenn nach dem Boden zunächst das ganze  Spantenskelett erstellt wird) zu verstehen

**[Bolzen]{#Bolzen}**
:    bolt (engl.), bolt (dk.)
:    Langer Metallstift zur Befestigung struktureller Elemente

**[Boot]{#Boot}**
:    kleineres Wasserfahrzeug, je nach Definition unter 12 m [→Länge](#Lua) und ohne [→Deck](#Deck)

**[Brasse]{#Brasse}**  
:    [→Tau](#Tau) an einer [→Rahnock](#Rahnock), um die [→Rahen](#Rah) horizontal am [→Mast](#Mast) zu drehen

**[brassen]{#brassen}**  
:    Drehen der [→Rahen](#Rah) in horizontaler Richtung entsprechend des Windeinfallwinkels

**[Breite eines Schiffs]{#Breite}**  
:    beam (engl.)       

**[Brigantine]{#Brigantine}**  
:    = [→Schonerbrigg](#Schonerbrigg)

<!-- 
**[Bronzezeitlicher Schiffbau Brittaniens]{#SchiffbauBZ-Brit}**
:    xxxxx (engl.)
:    archäologisch definierte Schiffsbautradition mit folgenden Merkmalen: xxxxxxxxxxxxxxxxx

**[Bronzezeitlicher Schiffbau Nordeuropas]{#SchiffbauBZ-Nord}**
:    xxxxx (engl.)
:    archäologisch definierte Schiffsbautradition mit folgenden Merkmalen: xxxxxxxxxxxxxxxxx
--> 

**[bündig gelegt]{#buendig}**
:    flush-laid (engl.)
:    die Hölzer sind ohne Fuge und nicht versetzt aneinander gelegt

**[Bug]{#Bug}**  
:    bow  (engl.), bov (dk.)
:    Vorderer Teil des Schiffs   

**[Bugband]{#Bugband}**  
:    breast-hook, crutch (engl.), bovbånd, stævenknæ (dk.)
:    V-förmiger [→Balken](#Balken) oder Querbalken, der die beiden Bordwände im Bereich des [→Vorstevens](#Vorsteven) verbindet. Im [→Heck](#Heck) als *Heckband*

**[Bugspriet]{#Bugspriet}**  
:    Schräg nach vorn ragender Vormast

**[Bugstag / Backstag / Seitenstag]{#Bugstag}**  
:    Seitliche Verstagung am [→Bugspriet](#Bugspriet)

**[Bullauge]{#Bullauge}**  
:    Rundes Schiffsfenster

**[Bullentalje]{#Bullentalje}**  
:    Sicherheitstalje von der [→Nock](#Nock) des [→Großbaums](#Grossbaum) nach vorn geriggt


## C

[<Inhalt](#inhalt)

## D

[<Inhalt](#inhalt)

**[Deck]{#Deck}**  
:    deck (engl.), dæk (dk.)
:    Horizontaler Plankenboden der die unterschiedlichen Ebenen des Schiffs definiert

**[Decksbalken]{#Decksbalken}**  
:    deck beam (engl.), dæksbjælke (dk.)
:    [→Querbalken](#Querbalken), der das [→Deck](#Deck) trägt

**[Deckssprung]{#Deckssprung}**  
:    sheer (engl.), spring (dk.)
:    längs verlaufende Wölbung eines Decks zwecks Wasserablauf

**[Deutel]{#Deutel}**  
:    square wedge (engl.), duttle (dk.)      
:    vierkantiger [→Keil](#Keil) eines Holznagels

**[Dirk]{#Dirk}**  
:    Haltetau des [→Baumes](#Baum)

**[dirken]{#dirken}**  
:    Anheben oder Senken des [→Baumes](#Baum)

**[Drehdavit]{#Drehdavit}**  
:    Kleiner galgenförmiger Drehkran

**[Dollbord, Strieme, Sielbord]{#Dollbord}**  
:    wale, gunwhale (engl.), ræling, essing (dk.)
:    Oberster, verstärkter [→Plankengang](#Plankengang) der Bordwand      

**[Dollen, Keipen]{#Dollen}**  
:    rowlock, thole-pin (engl.), åretol, årefæste, tollepind (dk.)
:    Widerlager für die [→Riemen](#Riemen), auf dem [→Dollbord](#Dollbord) befestigt

**[Ducht]{#Ducht}**  
:    thwart (engl.), tofte (dk.)    
:    Quer zum [→Kiel](#Kiel) verlaufender [→Balken](#Balken), der als Sitzbank dient

**[Dübel, Stift, Zapfen]{#Duebel}**  
:    dowel (engl.), peg (engl.)  
:    Stift mit einseitigem Kopf, das durch künstliche Verdickung der anderen Seite, etwa mit einem [→Keil](#Keil), im Bohrloch gehalten wird   

**[dumpen]{#dumpen}**  
:    Drehen der [→Rah](#Rah) in vertikaler Richtung

**[Dumper]{#Dumper}**  
:    [→Tau](#Tauwerk) am Ende der [→Rah](#Rah), um sie vertikal zu drehen

**[dwars]{#dwars}**
:    [→querab](#querab)

## E

[<Inhalt](#inhalt)

**[Eselshaupt]{#Eselshaupt}**
:    Verbindungsstück am oberen Ende eines Masttopps zwischen Untermast und Stenge

## F

[<Inhalt](#inhalt)

**[Fähre]{#Fähre}**
:    Schiffstyp zum übersetzen über ein Gewässer, meistens einen Fluss oder eine Bucht oder zu einer Insel

**[Fallen, Fall]{#Fallen}**
:    halyard (engl.), fald (dk.)
:    [→Leinen](#Leinen), die dem Setzen des [→Stagsegel](#Stagsegel) oder der [→Rahen](#Rah) dienen

**[falscher Kiel]{#fKiel}**
:    [→Kiel](#Kiel)

**[Fender]{#Fender}**
:    Gegenstand, der das Schiff vor dem Schäden durch die Pier schützt und an die Bordwand gehängt wird

**[fieren]{#fieren}**
:    Herunterlassen, Nachlassen oder Gleitenlassen einer Leine oder Kette

**[Fischung]{#Fischung}**
:    [→Mastfischung](#Mastfischung)

**[Fockmast]{#Fockmast}**
:    Vorderster senkrechter [→Mast](#Mast)

**[Focksegel]{#Focksegel}**  
:    Segel am [→Fockmast](#Fockmast)

**[Freibord]{#Freibord}**  
:    freeboard (engl.), bribord (dk.)
:    Vertikale Distanz der Wasseroberfläche zur niedrigsten Oberkanten der Bordwand

**[Fuß]{#Fuß}**
:    Unterer Teil eines [→[Stagsegels](#Stagsegel) oder [→Besansegels](#Besansegel)

**[Fußperd / Fußpeerd]{#Fussperd}**
:    [→Tau](#Tauwerk) unterhalb einer [→Rah](#Rah) zum Aufstützen der Füße, dient zum Ausentern in die Rah

## G

[<Inhalt](#inhalt)

**[Gaffel]{#Gaffel}**
:    Oben am [→Mast](#Mast) angebrachte oder heißbare, nach hinten aufwärtsragende Stange, an der Segel oder Flaggen angebracht werden

**[Gaffeldirk]{#Gaffeldirk}**
:    [→Tau](#Tauwerk) zum Einstellen der [→Gaffel](#Gaffel) in der Höhe

**[Gaffelgeer]{#Gaffelgeer}**
:    [→Leinen](#Leinen) zum seitlichen Einstellen der [→Gaffel](#Gaffel)

**[Gaffelliek]{#Gaffelliek}**
:    Oberes [→Tau](#Tauwerk) am [→Gaffelsegel](#Gaffelsegel)

**[Gaffelnock]{#Gaffelnock}**
:    Oberes Ende einer [→Gaffel](#Gaffel)

**[Gaffelsegel]{#Gaffelsegel}**
:    Viereckiges [→Segel](#Segel), das oben von einer [→Gaffel](#Gaffel) gehalten wird. bzw. zwischen [→Gaffel](#Gaffel), [→Baum](#Baum) und [→Mast](#Mast) aufgespannt ist

**[Gaffeltoppsegel]{#Gaffeltoppsegel}**
:    Oberstes [→Gaffelsegel](#Gaffelsegel) an einem [→Mast](#Mast)

**[Galeere]{#Galeere}**
:    galley (engl.)
:    Schiffstyp, gerudertes [→Kriegsschiff](#Kriegsschiff)


<!-- 
**[Gallorömischer Schiffbau]{#GalloroemischerSchiffbau}**
:    xxxxx (engl.)
:    archäologisch definierte Schiffsbautraditionen mit folgenden Merkmalen: Seegehendxxxxxxxxxxxxxxxxx Flussschiffe
 --> 
 
 
**[Gangway]{#Gangway}**
:    Mobiler Steg, der Schiff und Pier verbindet

**[Garn]{#Garn}**  
:    yarn (engl.)
:    Aus Fasern/Fäden gedrehte oder gesponnene dünnere [→Leinen](#Leinen)

**[Geer]{#Geer}**
:    Seil, welches das Ausschwingen (Auswehen) einer [→Gaffel](#Gaffel) minimiert
      
**[Geitau]{#Geitau}**
:    Leine zum Anholen der [→Schothörner von [→Rahsegeln an die [→Rahennocken](#Rah))nocken)

**[gezurrt]{#gezurrt}**  
:    lashed (engl.) 
:    Mit Leinen befestigt; [→zurren](#zurren) 

**[Glasen]{#Glasen}**
:    Läuten mit der Schiffsglocke zur Angabe der Uhrzeit: 30 min. nach Wachbeginn = 1 Glas (1 Glockenschlag), 1 h = 2 Glasen (2 Schläge), eine Wache hat 8 Glasen

**[Gording]{#Gording}**
:    Tau zum Aufholen des [→Segels](#Segel) an die [→Rah](#Rah)

**[Großbaum]{#Grossbaum}**
:    [→Baum](#Baum) am [→Großmast](#Grossmast) bzw. Segelstange am [→Unterliek](#Unterliek) des [→Großsegels](#Grosssegel)

**[Großmast]{#Grossmast}**
:    Hauptmast, größter [→Mast](#Mast)

**[Großsegel]{#Grosssegel}**
:    Unteres [→Segel](#Segel) am [→Großmast](#Grossmast)

## H

[<Inhalt](#inhalt)

**[Hals]{#Hals}**
:    Untere vordere Ecke von Dreiecksegeln, die mit dem Schiff fest verbunden wird bzw. [→Tau](#Tauwerk) an der [→Fock](#Focksegel), das am [→Schothorn befestigt ist und mit dem das Segel zusammen mit der [→Schot](#Schot) in den Wind gestellt wird

**[Halse]{#Halse}**
:    Das Schiff wird mit dem [→Heck](#Heck) durch den Wind gedreht

**[Halshorn]{#Halshorn}**  
:    Ecke eines [→Stagsegels](#Stagsegel), die im Winkel zwischen [→Mast](#Mast) und [→Baum](#Baum) liegt

**[Halsstande]{#Halsstande}**
:    Kurze Stander vom [Halshorn](#Halshorn) eines [→Stagsegels](#Stagsegel) nach unten zum [→Bugspriet](#Bugspriet)

**[Handstag / Handläufer]{#Handstag}**
:    Eisenstange zum Festhalten an [→Rahen](#Rah), am [→Bugspriet](#Bugspriet) sowie an Aufbauten

**[Hanf]{#Hanf}**  
:    hemp     
:    Material für [→Taue](#Tauwerk)   

**[Harz, Pech]{#Harz}**  
:    pitch (engl.)       
:    Abdichtungsmasse

**[Heck]{#Heck}**  
:    stern (engl.), agterskib, hæk (dk.)
:    Hinterer Teil des Schiffs    

*[Spiegelheck]{#Spiegelheck}* 
:    transom (engl.), spejl (dk.)
:    gerader ausgebildetes [→Heck](#Heck)

*[Spitzgatt]{#Spitzgatt}* 
:    spitz ausgebildetes [→Heck](#Heck)

*[Rundgatt]{#Rundgatt}* 
:    rund ausgebildetes [→Heck](#Heck)

**[Heckband]{#Heckband}** 
:    Entsprechung des [→Bugband](#Bugband)
 im [→Heck](#Heck)

**[heißen]{#heissen}**
:    Aufholen, anziehen einer SpitzgattLeine


<!-- 
**[Hellenistischer Schiffbau]{#HellenistischerSchiffbau}**
:    xxxxx (engl.)
:    archäologisch definierte Schiffsbautradition mit folgenden Merkmalen: xxxxxxxxxxxxxxxxx
 --> 



**[Helling, Helgen]{#Helling}**
:    stocks (engl.), bedding (dk.)
:    massive Balken als Unterlage für den Schiffsbau

**[holende Parten]{#holendeParten}**
:    Handläufer der [→Taljen](#Talje)
, an denen mit der Hand gezogen wird 


<!--  
**[Holk]{#Holk}**
:    xxxxxx (engl.)
:    archäologisch definierter Schiffstyp mit folgenden Merkmalen: xxxxxxxxxxxxxxxxx
--> 


**Holznagel**  
:    siehe [→Nagel](#Holznagel)    

*[kleiner Holznagel]{#kHolznagel}*
:    peg (engl.), pløk (dk.)   
:    [→Holznagel](#Holznagel) geringer Dimension, der entwa für die Verbindung von [→Planke](#Planke)n verwendet werden kann


## I

[<Inhalt](#inhalt)

## J

[<Inhalt](#inhalt)

**[Jackstag]{#Jackstag}**
:    Eisenstange zum Anbinden des [→Rahsegels](#Rahsegel) an die [→Rah](#Rah)

**[Jungfer, Juffer]{#Jungfer}**
:    dead-eye (engl.), jomfru (dk.)
:    [→Holzblock](#Block) mit Löchern für [→Leinen](#Leinen), etwa [→Wanten](#Wanten)

## K

[<Inhalt](#inhalt)

**[Kabelgatt]{#Kabelgatt}**
:    Lagerraum für [→Taue](#Tauwerk) etc.

**[Kalfat]{#Kalfat}**  
:    caulking (engl.)       
:    Abdichtungsmaterial, dass die Fugen zwischen den [→Planken](#Planke) abdichtet

*[eingelegtes Kalfat]{#eingelegtesKalfat}*  
:    caulking, inlaid chaulking (engl.), indlagt kalfatering (dk.)   
:    [→Kalfat, das in die in die [→Kalfatnut](#Kalfatnut) zwischen zwei [→Planken](#Planke) im [→Klinker](#Klinker)bau eingelegt wird bevor diese verbunden werden

*[eingepresstes Kalfat]{#eingepresstesKalfat}*  
:    luting (engl.), slået kalfatering  (dk)
:    [→[Kalfat](#Kalfat), das in die Fuge zwischen den [→Planken](#Planke) eingepresst wird, z. B. bei der [→verdeckten Kalfaterung](#vKalfaterung)

**[Kalfaterung]{#Kalfaterung}**  
:    caulking (engl.), kalfatering (dk.)       
:    Abdichtungsmaterial, dass die Fugen zwischen den [→Planken](#Planke) abdichtet bzw. die Tätigkeit des Abdichtens

*[verdeckte Kalfaterung]{#vKalfaterung}*  
:    covered caulking (engl.), inddækket kalfatering (dk.)       
:    [→Kalfaterung](#Kalfaterung), bei der das Kalfatmaterial ([→eingepresstes Kalfat](#eingepresstesKalfat)) in die Fuge zwischen den [→Planke](#Planke)n eingepresst wird und dann oft mit einer Leiste abgedeckt wird; z. b. typisch für [→Koggen](#Kogge1)

**[Kalfatleiste, Sintelrute]{#Kalfatleiste}**
:    caulking lath (engl.), liste (dk.)
:    Holzleiste, die das Abdichtungsmaterial ([→eingepresstes Kalfat](#eingepresstesKalfat)) in der Fuge hält

**[Kalfatnut]{#Kalfatnut}**
:    caulking cove (engl.), uldrille (dk.)
:    Eintiefung für die Aufnahme des Dichtungsmaterials ([→Kalfat](#Kalfat), [→eingelegtes Kalfat](#eingepresstesKalfat)) zwischen den [→Planke](#Planke)n

**[Kardeele]{#Kardeele}**
:    Einzelelemente aus denen jedes Seil oder [→Tau](#Tauwerk) besteht. Kardeel besteht aus 18-50 [→Garnen](#Garn) und 3-5 Kardeele fügen sich zu einer [→Trosse](#Trosse) zusammen

**[Kausch]{#Kausch}**
:    Metallschutz in den gespleißten [→Augen](#Auge) von [→Tauen](#Tauwerk)

**[Keil]{#Keil}**  
:    wedge (engl.), kile (dk.)      
:    Im Querschnitt dreieckiges Holzstück zum Aufweiten oder Füllen von Spalten oder weiten von [→Holznägeln](#Holznagel)

**[Kettenlänge]{#Kettenlaenge}**
:    Längenmaß der Ankerkette, 1 Kettenlänge entspricht 25 Meter

**[Kiel]{#Kiel}**  
:    keel (engl.), køl (dk.)   
:    Längsbalken der in der Mitte des Schiffsbodens die Hauptachse des Schiffes darstellt; nach dem Kielquerschnitt werden die Typen P ([→Kielplanke](#Kielplanke)), T, Y, U und R (rabbeted) unterschieden.

*[Loskiel, falscher Kiel]{#Loskiel}*
:    false keel (engl.), stråkøl (dk.)
:    Balken, der außen auf dem [→Kiel](#Kiel) zu dessen Schutz und Erhöhung befestigt ist

*[Kielplanke, Kielbohle, Plankenkiel, Bohlenkiel]{#Kielplanke}*
:    keel plank (engl.), plankenkøl (dk.)
:    Längsplanke die in der Mitte des Schiffsbodens die Hauptachse des Schiffes darstellt; im Gegensatz zu einem "richtigen" Kiel hat sie eine Höhe die den anderen Planken annähernd entspricht oder geringfügig größer ist; sie kann auch im Gegensatz zum Kiel die Abdrift nicht minimiere
 
**[Kielgang]{#Kielgang}**  
:    garboard (engl.), kølbord (dk.)      
:    [→Plankengang](#Plankengang) der direkt am [→Kiel](#Kiel) anliegt 

**[Kielschwein]{#Kielschwein}**  
:    keelson (engl.),  kølsvin (dk.) 
:    [→Balken](#Balken), der längs über dem [→Kiel](#Kiel) liegt, oft sehr massiv ist und [→Mastspur](#Mastspur) darstellt. 

**[Kielschweinknie]{#Kielschweinknie}**  
:    buttress (engl.), kølsvin-knæ (dk.)    
:    [→Knie](#Knie), welches zur Fixierung des [→Kielschweins](#Kielschwein) beiträgt

**[Kielsprung, Kielbucht]{#Kielsprung}**  
:    rocker (engl.), kølbugt (dk.)   
:    Aufwärtskrümmung des Kielverlaufs 

**[Kimm]{#Kimm}**  
:    turn of the bilge, chine (engl.), remmen, kiming (dk.)   
:    Übergang zwischen Schiffsboden und Bordwand

**[Klampe]{#Klampe}**
:    cleat, clamp (engl.)
:    Objekt mit zwei Hörnern zum Belegen mit [→Tauwerk](#Tauwerk)

**[Klau]{#Klau}**
:    Mastseitiges, früher gabelartiges Ende der [→Gaffel](#Gaffel) oder des [→Baums](#Baum)

**[kleiden]{#kleiden}**
:    Umwickeln mit [→Schiemannsgarn](#Schiemannsgarn), oft wird die Kleidung zur Wetterfestmachung geölt, geteert oder mit Farbe bestrichen

**[Kleidung]{#Kleidung}**
:    Das Stück eines Seils oder Taus, welches mit [→Schiemannsgarn](#Schiemannsgarn) umwickelt ist

**[Klinker]{#Klinker}**  
:    clinker (engl.) , klinker (dk.)
:    Bauweise, bei der die [→Planke](#Planke)n überlappend angeordnet sind und direkt, oft mit [→Eisennieten](#Niet) verbunden sind; die obere [→Planke](#Planke) überlappt die untere auf der Außenseite. Andernfalls spricht man von inverser Klinkerung.

*[klinkergebaut]{#klinkergebaut}*
:    clinker-build (engl.), klinkerbygget (dk.)   
:    In Klinkerbauweise ([→Klinker](#Klinker)) hergestellte Schale

**[Klumpblöcke]{#Klumpbloecke}**
:    [→Blöcke](#Block) mit gerundeten Kanten, (z.B. bei den Vorsegelschoten)

**[Klüse]{#Kluese}**
:    Geschlossene oder auch oben offene, gerundete Öffnung zur Führung von Tauwerk oder Ketten

**[Klüverbaum]{#Klueverbaum}**
:    Stenge-Verlängerung eines älteren [→Bugspriets](#Bugspriet)

**[Knebel]{#Knebel}**
:    toggle (engl.)
:    Holzstück bzw. Knopf, dass das Durchrutschen einer Leine durch eine Öse verhindert

**[Knie]{#Knie}**  
:    knee (engl.), knæ (dk.)       
:    Gewinkeltes Holz, oft aus einem [→Krummholz](#Krummholz) hergestellt, das verwendet wird, um zwei gewinkelte Objekte miteinander zu verbinden

**[Knorr]{#Knorr}**
:    knarr (engl.), knarr (dk.)
:    Schiffstyps, altnordische Bezeichnung eines [→Lastschiff](#Lastschiff)

**[Knoten]{#Knoten}**
:    Geschwindigkeitsmaß (Seemeile je Stunde) 1 kn = 1 sm/h = 0,514 m/s

**[Kogge 1]{#Kogge1}**
:    cog (engl.)
:    archäologisch definierter Schiffstyp mit folgenden Merkmalen: Koggennägel, Harte Kimm, flacher, kraweelgebauter Boden, geklinkerte Bordwand
<!-- 
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 --> 
 
**[Kogge 2]{#Kogge2}**
:    cog (engl.)
:    historisch belegter Name eines Schiffstyps, der in Texten und auf Abbildungen vorkommt; Es ist nicht immer klar, ob eine historische Nennung einer [→Kogge 1](#Kogge1) auch einer archäologischen Kogge entspricht

**[Kopf]{#Kopf}**  
:    Obere Ecke eines [→Stagsegels](#Stagsegel), in der das Fall und der Niederholer befestigt sind

**[Kombüse]{#Kombuese}**
:    Schiffsküche

**[Kontaktfläche, Auflage]{#Kontaktflaeche}**
:    table (engl.), anlægflade (dk.)
:    Oberfläche über sich sich zwei Hölzer in einer [→Schäftung](#Schäftung) berühren

**[Kraweel, Karweel]{#Kraweel}**
:    carvel (engl.), kravel (dk.)
:    Bauweise, bei der die [→Planke](#Planke)n auf Stoß aneinander liegen

**[kraweelgebaut]{#kraweelgebaut}**
:    flush-laid, carvel-built (engl.), glat, kravellagt, kravelbygget (dk.)
:    In [→Kraweelbauweise](#Kraweel) gebaut

**[Kriegsschiff]{#Kriegsschiff}**
:    Schiffstyp für militärische Einsätze

**[Krummholz]{#Krummholz}**
:    crook  (engl.), krumtømmer (dk.)     
:    Natürlich gewachsenes, gewinkeltes Holz



## L

[<Inhalt](#inhalt)

**[Länge über alles]{#Lua}**  
:    length overall (engl.), længde overalt (dk.)    
:    Länge von [→Vorsteven](#Vorsteven) zu [→[Achtersteven](#Achtersteven)

**[Langschiff]{#Langschiff}**
:    Schiffstyp, [→Mannschaftsboot](#Mannschaftsboot) im [→Nordischen Schiffbau](#NordischerSchiffbau)

**[Längsriss]{#Laengsriss}**  
:    sheer plan (engl.), opstalt (dk.)    
:    Graphische Liniendarstellung der Außenkante des Schiffsrumpfes von der Seite gesehen

**[Lannung, Landung]{#Lannung}**  
:    land, lap (engl.), land (dk.)    
:    Oberfläche im Überlappungsbereich zweier [→Planken](#Planke) im [→Klinkerbau](#Klinker) 

**[Lasche]{#Lasche}**  
:    scarf (engl.), scarph (engl.)     
:    Überlappende Verbindung (Überblattung, [→Schäftung]{#Schäftung)) zweier Hölzer, wobei die Dicke der einzelnen Teile nicht überschritten wird

*[Lasche mit Absatz]{#LaschemitAbsatz}*  
:    hooked scarf (engl.)       
:    [→Lasche](#Lasche) mit einem Absatz/einer Stufe zu besseren Verzahnung

*[einfache Lasche]{#einfacheLasche}*  
:    stop scarf (engl.), platlask (dk.)      
:    [→Lasche](#Lasche) mit flachen und geraden Plankenenden

**[Lasching]{#Lasching}**
:    Breites Band zum Festmachen des Segels

**[Lastschiff, Frachtschiff´]{#Lastschiff}**
:    Schiffstyp für den Transport von Gütern

**[Lateinersegel]{#Lateinersegel}**  
:    latin sail (engl.)
:    Ein dreieckiges [→Schratsegel](#Schratsegel), das mit einem Rutenliek an der Rute befestigt ist 

**[Laufendes Gut]{#LaufendesGut}**
:    Das gesamte bewegliche [→Tauwerk](#Tauwerk) an Bord eines Schiffes

**[Lecksegel]{#Lecksegel}**
:    Großes verstärktes Tuch zum Abdichten von Lecks

**[Lee]{#Lee}**
:    Dem Wind abgewandte Seite

**[Leesegel]{#Leesegel}**
:    Schönwettersegel, die an besonderen Leesegelspieren an den Seiten von [→Rahsegeln](#Rahsegel) gesetzt werden

**[Legel / Lögel / Stagreiter]{#Legel}**
:    Stahl- oder Holzringe mit denen das [→Stagsegel](#Stagsegel) auf dem [→Stag](#Stag) gleitet

**[Leinen]{#Leinen}**
:    [→Tau](#Tauwerk) mittleren Durchmessers

**[Leichter]{#Leichter}**
:    Schiffstyp um die Ladung größerer Frachtschiffe in flachen Küstenbereichen zwischen [→Reede](#Reede) und Land zu transportieren

**[Leiste]{#Leiste}**  
:    batten (engl.), revle, liste (dk.)     
:    Langschmales Holz, meist als Auflage über einer [→Naht](#Naht1) verwendet

**[Leitwagen / Leuwagen]{#Leitwagen}**
:    an Deck querliegender, flacher Stahlrohrbügel, an dem der untere Schotenblock bei Wendemanövern von Bordseite zu Bordseite gleiten kann

**[Liek]{#Liek}**
:    [→Tau](#Tauwerk) rundum an den Kanten eines [→Segels](#Segel)

*[Unterliek, Fußliek]{#Unterliek}*  
:    [→Liek](#Liek) an der Unterseite des [→Segels](#Segel)

*[Oberliek, Gaffelliek, Rahliek]{#Oberliek}*
:    [→Liek](#Liek) an der Oberseite des [→Segels](#Segel)

*[Vorliek, stehendes Liek]{#Vorliek}*
:    [→Tau](#Tauwerk) am [→Stag](#Stag) eines  [→Stagsegels](#Stagsegel) oder die mastseitigen [→Liek](#Liek) der  [→Großsegel](#Grosssegel) bzw. [→Gaffeltoppsegel](#Gaffeltoppsegel)

*[Seitenliek]{#Seitenliek}*
:   [→Liek](#Liek) an der Seite des Segels

**[Lippe]{#Lippe}**
:    Offenes Metallauge an Deck oder auf dem Schanzkleid zum Führen von [→Tauwerk](#Tauwerk)

**[Lögel]{#Loegel}**
:    Offene Öse, mit denen [→Stagsegel](#Stagsegel) mit den  [→Stagen](#Stag) beweglich verbunden werden

**[loggen]{#loggen}**
:    Messen der Schiffsgeschwindigkeit

**Loskiel**
:    [→Kiel](#Loskiel)

**[Lot 1]{#Lot1}**
:    lot (engl.), lot (dk.)
:    Übergangsstück zwischen  [→Kiel](#Kiel) und  [→Steven](#Steven)

**[Lot 2]{#Lot2}**
:    lead (engl.)
:    Gewicht zum [→loten](#loten)

**[loten]{#loten}**
:    Messen der Wassertiefe

**[Lümmellager]{#Luemmellager}**
:    Stützgelenk am [→Mast](#Mast) für Lade- und Segelbäume

**[Luggersegel]{#Luggersegel}**
:    lug sail (engl.), luggersejl (dk.)
:    [→Segel](#Segel) ähnlich dem [→Lateinersegel](#Lateinersegel), wobei der untere Teil der [→Spiere](#Spiere) stark verkürzt ist

**[Luv]{#Luv}**
:    Dem Wind zugewandte Seite


## M

[<Inhalt](#inhalt)

**[Mallen]{#Mallen}**  
:     [→Spant](#Spant)artige Schablonen für den Bau des Schiffsrumpfes, die nach Fertigstellung entfernt werden

**[Mannschaftsboot]{#Mannschaftsboot}**
:    Schiffstyp für den Transport von Truppen, Variante des [→Kriegsschiffs](#Kriegsschiff)

**[Marsstenge]{#Marsstenge}**
:    Stenge-Verlängerung, die an der Vorderkante vom Masttopp des Untermastes ansetzt

**[Mast]{#Mast}**  
:    mast (engl.)
:    Senkrechtes Rundholz zum halten der [→Segel](#Segel)

*[Untermast]{#Untermast}*
:    Unterster Teil vom [→Mast](#Mast), reicht vom [→Mastfuß](#Mastfuss) bis zum [→Eselshaupt](#Eselshaupt) am Masttopp

**[Mastfall]{#Mastfall}**
:    Schrägstellung eines [→Mast](#Mast) in der Seitenansicht nach hinten

**[Mastfischung]{#Mastfischung}**  
:    mast fish (engl.), mastfisk (dk.)     
:    Befestigungseinrichtung des [→Mastes](#Mast)

**[Mastfuß]{#Mastfuss}**
:    mast step (engl.)
:    Unterster Teil eines [→Mastes](#Mast), der innerhalb des Rumpfes in der [→Mastfischung](#Mastfischung) auf dem [→Kiel](#Kiel) steht

**[Mastspur]{#Mastspur}**  
:    mast step (engl.), mastespor (dk.)      
:    Aufnahmestelle für den [→Mastfuß](#Mastfuss) im [→Kielschwein, [→Mast](#Mast) [→Spant](#Spant) oder [→Sporholz](#Sporholz)

*[Mastspant]{#Mastspant}*
:    [→Spant](#Spant) mit einem Loch als [→Mastspur](#Mastspur)

**[Mastgarten]{#Mastgarten}**
:    [→Nagelbänke](#Nagelbank) um die [→Masten](#Mast) herum mit allen [→Taljen](#Talje) , [→Blöcken](#Block) und [→Tampen](#Tampen)

**[Mastkragen]{#Mastkragen}**
:    Dichtung an der Durchdringungsstelle des [→Mastes](#Mast) am [→Deck](#Deck)

**[Masttopp]{#Masttopp}**
:    Maststück von der [→Saling](#Saling) bis zum [→[Eselshaupt](#Eselshaupt)

**[Meginhufr]{#Meginhufr}**  
:    meginhufr (engl.), meginhufr (dk.)
:    starke [→Übergangsplanke](#Uebergangsplanke) im [→Nordischen Schiffbau](#NordischerSchiffbau)

**[Messe]{#Messe}**
:    Raum, in dem die Besatzung die Mahlzeiten einnimmt

**[mittschiffs]{#mittschiffs}**
:    amidships (engl.), midskibs (dk.)
:    Indiziert einen Punkt in der Mitte des Schiffes, in Hinsicht auf die Länge des Schiffes

**[Musing]{#Musing}
:    Sicherung eines Schäkelbolzens gegen selbständiges Herausdrehen mittels weichen Drahtes


## N

[<Inhalt](#inhalt)

**[Nagel]{#Nagel}**  
:    spike, nail (engl.), spiger, søm (dk.)
:    Metall- oder Holzstift mit verdicktem Kopf, dass zwei Objekte miteinander verbindet, indem der Nagelkopf das eine Objekt und entweder die Reibung des Nagelschaftes oder die umgebogene Spitze bwz. die verdickte Spitze das andere Objekt hält. Wir verwenden den Begriff generisch, so dass er neben einfachen Metallnägeln auch Holzdübel oder den Nagelteil von Eisennieten umfassen kann

*[umgebogener Nagel]{#uNagel}*
:    clench-nail, (engl.)
:    [→Nagel](#Nagel), dessen Ende einfach oder doppelt umgebogen wurde um das Herausgleiten zu verhindern

*[doppelt umgebogener Nagel]{#duNagel*
:    double-bent nail  (engl.), vegnet spiger (dk.)
:    Nagel, dessen Ende doppelt umgebogen wurde um das Herausgleiten zu verhindern; diese Nägel sind u. a. typisch für Koggen

*[Klinkernagel, Niet]{#Klinkernagel}*
:    rivet (engl.), klinknagel (dk.)
:    = [→Niet](#Niet), [→Nagel](#Nagel), dessen Ende über einer [→Klinkerplatte](#Nietplatte) platt geklopft wurde; nach ihrem Querschnitt werden runde und quadratische Nietnägel unterschieden; Niete oder Klinkernägel sind typisch für die Plankenverbindungen der nordischen Schiffe

*[Holznagel, Holzdübel]{#Holznagel}*
:    treenail (engl.), trænagle (dk.)
:    [→Nagel](#Nagel), der aus Holz besteht, einen verdicken Kopf hat und auf der anderen Seite durch einen [→Keil](#Keil) aufgespreizt wird; nach ihrer Kopfform werden konkave und zylindrische Holznägel unterschieden; Holznägel werden regelhaft für die Verbindung der Planken mit den Spaten, der einzelnen  [→Spantteile](#Spant) miteinander und gelegentlich, besonders im Mittelalter an der Südküste der Ostsee für die Verbindung der Planken untereinander verwendet

**[Nagelbank]{#Nagelbank}**
:    Waagerechter Balken mit Bohrungen zum Einstecken von Belegnägeln und zum Belegen der [→holenden Parten](#holendeParten) des [→Laufenden Gutes](#LaufendesGut)

**[Naht 1]{#Naht1}**  
:    seam (engl.), søm, samling, nåd (dk.)     
:    Befestigung zweiter [→Planken](#Planke) an ihren Längsseiten im Gegensatz zur [→Schäftung](#Schäftung)

**[Naht 2{#Naht2}**  
:    stitch (engl.), 
:    Technik der Plankenverbindung mit Hilfe von Leinen, die durch Löcher in den [→Planken](#Planke) geführt werden

**[Niederholer]{#Niederholer}**
:    [→Leinen](#Leinen), die dem schnellen Bergen von [→Segeln](#Segel) dienen

**[Niedergang]{#Niedergang}**
:    Treppe auf Schiffen

**[Niet]{#Niet}**  
:    rivet, clench-nail (engl.), klinknagel (dk.)   
:     = [→Klinkernagel](#Klinkernagel), Metallstift mit verdicktem Kopf auf der einen Seite und einem über einer [→Nietplatte](#Nietplatte) plattgehämmertem Ende, der zwei Objekte, meist Planken, verbindet

**[Nietplatte]{#Nietplatte}**  
:    rove (engl.), klinkplade (dk.)       
:    Metallplatte, die auf den [→[Nietnagel](#Niet) oder [Klinkernagel](#Nagel) geschoben wird und vom plattgehämmerten Ende des Niets als Gegenstück zum Kopf gehalten wird.

**[Nock]{#Nock}**
:    Äußerste Spitze einer [→Rah](#Rah), [→Gaffel](#Gaffel) oder eines [→Baumes](#Baum)

*[Rahnock]{#Rahnock}*
:    [→Nock](#Nock) bzw. Ende einer Rah

**[Nockgording]{#Nockgording}**
:    Leinen, die die Mitte des [→Seitenlieks](#Seitenliek) eines [→Rahsegels](#Rahsegel) an die [→Rah](#Rah) holen

**[Nockhorn, Piek]{#Nockhorn}**
:    Ecke eines [→Segels](#Segel), die an einer [→Nock](#Nock) anliegt bzw. befestigt ist

**[Nockpeerd]{#Nockpeerd}**
:    [→Tau unterhalb der [→Nock](#Nock) der [→Rah](#Rah) zum Entern in die Rah



<!-- 
**[Nordischer Schiffbau]{#NordischerSchiffbau}**
:    punt (engl.)
:    archäologisch definierte Schiffsbautradition mit folgenden Merkmalen: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 --> 
 
 
 
**[Nüstergatt]{#Nuestergatt}**
:    limber hole (engl.), spygat (dk.)
:    Aussparung an der Unterseite eines [→Spants](#Spant) als Wasserdurchlass zwischen den [→Spantfächern](#Spant)

**[Nut- und Federverbindung]{#NutundFeder}**
:    Zwischenstagen (engl.), 
:    Verbindung von Holzteilen, bei der die Feder in die Nut der zu verbindenen Teile gesteckt wird; Für die Plankenverbindung ist diese Technik typisch für den Mediterranen Schiffbau

## O

[<Inhalt](#inhalt)

**Oberliek**  
:    siehe [→Liek](#Oberliek)

**[Ösfaß]{#Oesfass}**  
:    bailer (engl.), øskar (dk.)     
:    Behälter um Wasser aus dem Fahrzeug auszuschöfen

## P

[<Inhalt](#inhalt)

**[Paddel]{#Paddel}**  
:    paddle (engl.)
:    Stange mit flachem Ende (Blatt), das zum Antrieb verwendet wird, wobei das Blatt so durch das Wasser gezogen wird, dass eine Hand als Widerlager dient und der Paddeler in Fahrtrichtung sitzt

**[paddeln]{#paddeln}**  
:    Antreiben eines Bootes mit [Paddeln](#Paddel), wobei in Fahrrichtung gesessen und das Paddel mit beiden Händen ohne Widerlager am Boot durch das Wasser gezogen wird

**[Pantry]{#Pantry}**
:    Raum zur Geschirreinigung und -aufbewahrung

**[Pardune]{#Pardune}**
:    Abspannung der [→Masten](#Mast) seitlich nach hinten ähnlich den [→Wanten](#Wanten)

**[Piek]{#Piek}**
:    vorderster bzw. hinterster Raum eines Schiffes, in dem meistens Ballastwasser gefahren wird

*[Vorderpiek]{#Vorderpiek}*
:    vorderes [→Piek](#Piek)

*[Achterpiek]{#Achterpiek}*
:    hinteres [→Piek](#Piek)

**[Planke]{#Planke}**  
:    plank (engl.), bord, planke (dk.)     
:    Langes und dünnes Holz, das einen Teil der Schiffshülle darstellt

*[radial gespaltene Planke]{#rPlanke}*
:    radially cleft plank (engl.), spejlkløvet planke (dk.)
:    [→Planke](#Planke), die aus dem Baumstamm durch Spalten in "Keile" durch den Mittelpunkt entstanden sind

*[tangential gespaltene Planke]{#tPlanke}*
:    tangentially cleft plank (engl.), tangentielt kløvet planke (dk.)
:    [→Planke](#Planke), die aus dem Baumstamm durch Spalten in "Scheiben" entstanden sind

*[gesägte Planke]{#sPlanke}*
:    sawn plank (engl.)
:    [→Planke](#Planke), die aus dem Baumstamm tangential gesägt wurde

*[Beplankung, Außenhaut]{#Beplankung}*
:    Menge aller [→Bordwandplanken](#Planke)

*[Bordwandplanken]{#Bordwandplanken}*
:    [→Planken](#Planke) der Schiffshülle im Gegensatz zu [→Decksplanken](#Decksplanken)

*[Decksplanken]{#Decksplanken}*
:    [→Planken](#Planke) eines Decks im Gegensatz zu [→Bordwandplanken](#Bordwandplanken)

**[Plankenende]{#Plankenende}**
:    hood-end (engl.), bordhals (dk.)       
:    Ende einer [→Planke](#Planke)

**[Plankengang]{#Plankengang}**  
:    strake (engl.), bordgang, rang (dk.)     
:    Reihe ggf. längs verbundenen [→Planken](#Planke), die vom [→Kiel](#Kiel) an gezählt werden

**[toter Gang, verlorener Gang]{#toterGang}**  
:    stealer (engl.), indskudsrang, indstiksrang (dk.)     
:    kurzes Plankenstück, dass eine Lücke zwischen zwei [→Plankengängen](#Plankengang) ausfüllt

**[Poller]{#Poller}**
:    An Deck oder auf der Pier befestigte, meist zylinderförmige Vorrichtung zum Belegen von Festmacherleinen

**[Prahm]{#Prahm}**
:    punt (engl.)
:    Schiffstyp mit flachem, kiellosen Boden und einer harten Kimm


## Q

[<Inhalt](#inhalt)

**[Quarterdeck]{#Quarterdeck}**
:    quarter deck (engl.)
:    Erhöhtes [→Deck](#Deck) am [→Achterschiff](#Achterschiff)

**[querab, dwars]{#querab}**  
:    abeam (engl.), tværskibs (dk.)
:    Verweis auf einen Punkt etwa rechtwicklig zur Kielachse außerhalb des Schiffes

**[Querbalken]{#Querbalken}**  
:    crossbeam (engl.), tværbjælke (dk.)      
:    Quer zum [→Kiel](#Kiel) verlaufender [→Balken](#Balken)

*[Worp, Heckbalken]{#Worp}* 
:    transom (engl.), hæk-bjælke (dk.)      
:    [→Querbalken](#Querbalken) am [→Heck](#Heck)

## R

[<Inhalt](#inhalt)

**[Rah]{#Rah}**
:    yard (engl.), rå (dk.)
:    runde Stange, die horizontal querschiffs und drehbar am [→Mast](#Mast) befestigt ist und zur Befestigung von [→Segeln](#Segel) dient

**[Rahfall, Drehreep]{#Rahfall}**
:    [→Leine](#Leinen) zum Heißen[→Heißen](#heissen) und Fieren[→Fieren](#fieren) von [→Rahen](#Rah)

**[Rahsegel]{#Rahsegel}**
:    square sail (engl.)
:    [→Segel](#Segel), die im Gegensatz zu den [→Schratsegeln](#Schratsegel) in Schiffsquerrichtung stehen und an der [→Rah](#Rah) befestigt sind

**[Rammbordleiste]{#Rammbordleiste}**
:    Leiste, die außen oberhalb der Wasserlinie zum Schutz der Planken vor Berührungen durch Dalben etc. angebracht wird

**[Ramme]{#Ramme}**  
:    Metallenes Objekt, das am Bug angebracht ist und dem Rammen feindlicher Schiffe dient

**[Reede]{#Reede}**  
:    Wasserfläche außerhalb des Hafens, in der Schiffe Ankern können; in der Regel eine Warteposition

**[Reffbändsel]{#Reffbändsel}**
:    Bändsel am [→Segel](#Segel) zur Verkleinerung der Segelfläche

**[reffen]{#reffen}**
:    Teilweises Wegnehmen des [→Segels](#Segel), dient zur Verkleinerung der Segelfläche

**[Refflegel]{#Refflegel}**
:    Kurze Spleißaugen an einem [→Liek](#Liek)

**[Refftau, -talje]{#Refftau}**
:    [→Tau](#Tauwerk) zum teilweisen Wegnehmen des [→Segels](#Segel)

**[Riemen]{#Riemen}**  
:    oar (engl.), åre (dk.)    
:    Stange mit einem verbreiterten Ende (Blatt), das für den Antrieb mittels [→Rudern](#Rudern) verwendet wird

**[Riemengriff]{#Riemengriff}**  
:    loom (engl.), lom, håndtag på åre (dk.)    
:    Griff zur Handhabung eines [→Riemens](#Riemen)

**[Riemenpforte]{#Riemenpforte}**  
:    oarhole, oar port (engl.), årehul (dk.)
:    Öffnung für die [→Riemen](#Riemen) im [→Dollbord](#Dollbord)

**[Riemenpfortenverschluss]{#Riemenpfortenverschluss}**  
:    oarhole-lock (engl.), årehulslukke (dk.)
:    Abdeckung der [→Riemenpforte](#Riemenpforte)

**[Rigg]{#Rigg}**
:    rig (engl.), rigning (dk.)
:    Gesamtheit der [→Takelage](#Takelage)

*[laufendes Gut]{#laufendesGut}*
:    standing rigging (engl.)
:    = bewegliche Teile des [→Rigg](#Rigg)

*[stehendes Gut]{#stehendesGut}*
:    running rigging (engl.), stay (engl.), Stag (deu.)
:    = unbewegliche Teile des [→Rigg](#Rigg)

**[Ruder]{#Ruder}**  
:    rudder (engl.), ror (dk.)
:    Steuereinrichtung

*[Seitenruder]{#Seitenruder}*
:    side rudder (engl.), sideror (dk.)
:    [→Ruder](#Ruder), das an der Seite, meist [→Steuerbord](#Steuerbord) angebracht ist

*[Heckruder, Stevenruder]{#Heckruder}*
:    sternpost rudder (engl.), stævnror (dk.)
:    [→Ruder](#Ruder), das mittig am [→Heck](#Heck) angebracht ist

**[Rudern]{#Rudern}**
:    rowing (engl.)
:    Antrieb bei dem ein [→Riemen](#Riemen), wobei gegen die Fahrrichtung gesessen wird und der Riemen mit Hilfe eines Widerlagers, der [→Dolle](#Dollen) durch das Wasser gezogen wird

**[Ruderpinne]{#Ruderpinne}**  
:    tiller (engl.), rorpind (dk.)      
:    Hebel zum bedienen des [→Ruders](#Ruder) 

**[Ruderspant]{#Ruderspant}**  
:    rudder frame (engl.), ror [→Spant](#Spant) (dk.)     
:    [→Spant](#Spant) an dem das [→Ruder](#Ruder) befestigt ist

**[Rumpf]{#Rumpf}**  
:    hull (engl.), skrog (dk.)
:    Konstruktiver Körper des Schiffes ohne Rigg

**[Rundholz, Spieren]{#Rundholz}**  
:    spar (engl.), rundholt, spir (dk.)      
:    Stange mit rundem Querschnitt

**[Rute, lateinische Rah]{#Rute}**  
:    [→Spiere](#Spiere), die ein Lateinersegel hält, etwa mittig am Mast befestigt ist und schräg nach oben ausgerichtet ist


## S

[<Inhalt](#inhalt)

**[Saling]{#Saling}**
:    Arbeitsplattform im [→Mast](#Mast), die auch der Stabilität der Takelage[→Takelage](#Takelage) dient

**[Schäftung]{#Schäftung}**  
:    scarf, through scarf (engl.), skar, lask, bladlask (dk.)
:    Schäften bezeichnet das feste Zusammenfügen zweier Hölzer zu einem längeren, auf einer gemeinsamen Längsachse orientierten Holz, wobei die Enden oft abgeschrägt sind um eine möglichst große Kontaktfläche zu schaffen; der Überlappungsbereich heißt [→Lasche](#Lasche) und die sich berührenden Flächen [→Kontaktflächen](#Kontaktflaeche)

*[horizontale Schäftung/Lasche]{#[horizontaleSchaeftung}*
:    horizontal scarf (engl.), vandret lask (dk.)
:    [→Schäftung](#Schäftung) mit horizontaler [→Kontaktfläche](#Kontaktflaeche)

*[vertikale Schäftung/Lasche]{#vertikaleSchaeftung}*
:    vertical scarf (engl.), lodret lask, vertikal lask (dk.)
:    [→Schäftung](#Schäftung) mit vertikaler [→Kontaktfläche](#Kontaktflaeche)

*[Zungenschäftung]{#Zungenschaeftung}*
:    [→Schäftung](#Schäftung) mit in der Plankenmitte dünn auslaufender Zunge an der [→Kontaktfläche](#Kontaktflaeche)

*[Hakenlasche]{#Hakenlasche}*
:    hook scarf (engl.), hagelask (dk.)
:    [→Schäftung](#Schäftung) mit getreppter Oberfläche

**[Schalenbauweise]{#Schalenbauweise}**  
:    shell first, shell-based (engl.), skalbygget (dk.)
:    Bauweise, bei der zuerst die Hülle des Rumpfes aus [→Planke](#Planke)n zusammengesetzt wird und später das Spantenskelett eingesetzt wird, wobei dies auch schrittweise erfolgen und vorläufige Verbindungen einbeziehen kann. Diese Technik steht im Gegensatz zur [→Skelettbauweise](#Skelettbauweise)

**[schamfielen]{#schamfielen}**
:    Scheuern von [→Tauwerk](#Tauwerk) und [→Segeln](#Segel)

**[Schäkel]{#Schaekel}**
:    U-förmiges Verbindungsglied und Befestigungsglied aus Metall für [→Tauwerk](#Tauwerk) und Ketten

**[Schanzkleid, Schanzring]{#Schanzkleid}**
:    Erhöhung der Außenhaut um das Oberdeck

**[Schapp]{#Schapp}**
:    Schrank oder ein Schubfach an Bord

**[Scheiben]{#Scheiben}**
:    Seilrollen innerhalb von [→Blöcken](#Block) und Segelstangen

**[Scheibgatt]{#Scheibgatt}**
:    Längliche Öffnung in Masten usw. zur Lagerung einer Scheibe

**[Schergang]{#Schergang}**  
:    sheerstrake (engl.), essing (dk.)
:    Der oberste [→Plankengang](#Plankengang) unter dem [→Dollbord](#Dollbord) 

**[Schiff]{#Schiff}**
:    Wasserfahrzeug, größer als ein [→Boot](#Boot)

**[Schiemannsgarn]{#Schiemannsgarn}**
:    Dünnes, oft schon geteertes Takelgarn zum Kleiden und zum Setzen von Bändselungen und Taklings

**[Schlagpütz]{#Schlagpütz}**
:    Wassereimer mit am Henkel eingespleißtem Tampen zum einholen von Außenbordwasser

**[Schmiege]{#Schmiege}**
:    bevel (engl.), smig, affasning (dk.)
:    gewinkelte Auflagefläche eines Balkens zur Aufnahme eines anderen Holzes; die Schmiege eines  [→Spants](#Spant) muss so geartet sein, dass die Planken ohne Spalt anliegen können 

**[Schnigge]{#Schnigge}**
:    Snekkja (engl.), snekke (dk.)
:    Schiffstyp, offenes und flaches, gesegeltes Wasserfahrzeug, in der Wikingerzeit insbesondere ein kleines [→Langschiff](#Langschiff)

**[Schnitt, Längsschnitt]{#Schnitt}**
:    buttock line (engl), snit, længdesnit (dk.)
:    Graphische Liniendarstellung des Außenumrisses des Schiffes entlang von [→Kiel](#Kiel) und [→Steven](#Steven)

**[Schoner]{#Schoner}**
:    Schiffstyp, zwei- und mehrmastiges, ausschließlich mit [→Schratsegeln](#Schratsegel) getakeltes Segelschiff

**[Schonerbrigg]{#Schonerbrigg}**
:    Schiffstyp, Bezeichnung eines Schiffes nach seiner [→Takelung](#Takelage): vorderer Fockmast[→Fockmast](#Fockmast) mit Rahsegeln[→Rahsegeln](#Rahsegel) und hinterer Großmast[→Großmast](#Grossmast) nur mit [→Schratsegeln](#Schratsegel)

**[Schonersegel]{#Schonersegel}**
:    viereckiges [→Schratsegel](#Schratsegel) zwischen den [→Masten](#Mast) von [→Schonern](#Schoner)

**[Schot]{#Schot}**
:    [→Tau](#Tauwerk) zum Ausrichten der [→Segel](#Segel)

*[Rahsegelschot]{#Rahsegelschot}*
:    [→Tau](#Tauwerk), mit dem das [→Segel](#Segel) an die darunterliegende [→Rah](#Rah) geholt wird bzw.

*[Schratsegelschot]{#Schratsegelschot}*
:    [→Tau](#Tauwerk), mit dem das [→Segel](#Segel) in die gewünschte Richtung gestellt wird

**[Schothorn]{#Schothorn}**  
:    untere Ecken eines [→Rahsegel](#Rahsegel) bzw. Ecke eines [→Stagsegels](#Stagsegel), die am Ende des [→Baums](#Baum) liegt; am [→Schothorn](#Schothorn) sind die die [→Geitaue](#Geitau) und bei der [→Fock](#Focksegel) zusätzlich der Hals befestigt

**[Schott]{#Schott}**  
:    bulkhead (engl.), skot (dk.)       
:    vertikale Abtrennung von Rumpfsegmenten bwz.wasserdichte Wand im Schiffsrumpf oder Schiffstür  

**[Schratsegel]{#Schratsegel}**
:    [→Segel](#Segel), die im Gegensatz zu den [→Rahsegeln](#Rahsegel) in Schiffslängsrichtung stehen

**[Schwalbenschwanzverbindung]{#Schwalbenschwanzverbindung}**
:    dovetail joint (engl.), svalehale samling (dk.)
:    Eine Verbindung, bei der sich der Zapfen zum Ende hin weitet und damit ein herausgleiten aus dem Zapfloch zu verhindern

**[Schwenkdavit]{#Schwenkdavit}**
:    Bootsaussetzvorrichtung, die seitlich ausschwenkt

**[Schwert]{#Schwert}**  
:    senkrechte Platte, die die Abdrift vermindern soll und in Schiffsmitte oder an den Bordwänden ([→Seitenschwerter](#Seitenschwert)) angebracht sein kann

*[Seitenschwert]{#Seitenschwert}*
:    leeboard (engl.)
:    [→Schwert](#Schwert), das außen an der Bordwand angebracht ist, wobei sich an jeder Bordwand ein Schwert befindet; das [→leeseitige](#Lee) Schwert wird abgelassen und das [→luvseitige](#Luv) hochgezogen

**[schwojen]{#schwojen}**
:    Herumschwingen eines Schiffes um seinen Anker bei wechselnden Windrichtungen und Strömung

**[Seemeile]{#Seemeile}**
:    internationales Längenmaß in der Seefahrt, 1 sm = 1.852 m

**[Segel]{#Segel}**
:    sail (engl.)
:    Textilstück, dass auf einem Wasserfahrzeug aufgespannt wird, um mit Hilfe des Windes Vortrieb zu gewinnen

**[Segeln]{#Segeln}**
:    sailing (engl.)
:    Antriebstechnik, bei der [→Segel](#Segel) zum Einsatz kommen

**Seitenliek**  
:    siehe [→Liek](#Seitenliek)

**[Sintel, Kalfatklammer]{#Sintel}**
:    sintel (engl.), sintel, krampe (dk.)  
:    Metallklammer, welche die [→Kalfatleiste](#Kalfatleiste) hält.

**[Skelettbauweise]{#Skelettbauweise}**  
:    Bauweise, bei der zuerst das Spantenskelett erstellt wird, bevor die Planken angebracht werden. Diese Bauweise steht im Gegensatz zur [→Bodenbauweise](#Bodenbauweise)



<!-- 
**[Slawischer Schiffbau]{#SlawischerSchiffbau}**
:    punt (engl.)
:    archäologisch definierte Schiffsbautradition mit folgenden Merkmalen: xxxxxxxxxxxxxxxxx
 --> 
 
 
 
**[Snelle, Bietenstütze]{#Snelle}**  
:    snelle, beam-stanchion (engl.), snelle (dk.)
:    senkrechte Stütze unter der [→Bite](#Bite), oft mittig

**[Spant]{#Spant}**  
:    rib (engl.), frame (engl.), spant (dk.)
:    Holz oder Holzkonstruktion als Teil des  Spantengerüsts, das die innere Aussteifung des Schiffsrumpfes bewirkt und der jeweils einen Querschnitt des Rumpfes repräsentiert 

*[Hauptspant]{#Hauptspant}*
:    midship section (engl.), middel spant (dk.)
:    [→Spant](#Spant) an der breitesten Stelle des [→Rumpfes](#Rumpf)

*[Zwischenspant]{#Zwischenspant}*
:    intermediate frame, side frame (engl.), mellem spant, oplænger (dk.)
:    [→Spant](#Spant) an der nicht die ganze Bordwand abdeckt und oft nur aus einem [→Auflanger](#Auflanger) besteht, der zwischen zwei vollständigen  [→Spanten](#Spant) angebracht ist

**[Spantabstand]{#Spantabstand}**  
:    frame spacing (engl.), spantafstand (dk.)  
:    Abstand zwischen den [→Spanten](#Spant)

**[Spantdicke]{#Spantdicke}**  
:    siding (engl.), bredde af spant (dk.)  
:    Dicke eines [→Spants](#Spant) von der Seite an der Bordwand zu Oberkante gemessen  

**[Spantfach, Spantraum]{#Spantfach}**  
:    room (engl.),  spantrum, rum (dk.)
:    Raum zwischen zwei  [→Spanten](#Spant)

**[Spantriss]{#Spantriss}**  
:    body plan (engl.), spanterids (dk.)
:    Graphische Darstellung der Querschnitte des Schiffskörpfers als Außenlinien der  [→Spanten](#Spant)

**[Speigatt]{#Speigatt}**
:    limber hole (engl.), spygat (dk.)
:    Öffnung im Schanzkleid, damit überkommende Seen möglichst schnell wieder ablaufen können

**[Spiegel]{#Spiegel}**
:    Weitgehend gerader, teils gewölbter hinterer Rumpfabschluß

**Spiegelheck**
:    
:    [→Heck](#Spiegelheck)

**[Spiere]{#Spiere}**
:    spar (engl.)
:    Rundholz

**[Spill]{#Spill}**
:    Winde, die mittels Spaken oder Motorantrieb gedreht wird und mit der Tauwerk - bzw. Ankerketten bewegt werden

*[Gangspill]{#Gangspill}*
:    Senkrecht stehendes [→Spill](#Spill)

*[Pumpspill]{#Pumpspill}*
:    Waagerechtes [→Spill](#Spill)

**[Spillspake]{#Spillspake}**
:    Holz- oder Eisenstangen zum Drehen des [→Spills](#Spill) per Hand

**[Spleißauge]{#Spleißauge}**
:    [→gespleißtes](#spleißen) Auge am Ende eines Seils

**[spleißen]{#spleißen}**
:    Ineinanderflechten zweier [→Tauwerksenden](#Tauwerk)

**[Sponung, Falz]{#Sponung}**  
:    rabbet, rebate (engl.), spunding, rille (dk.)  
:    Aussparung für die Aufnahme eines [→Plankenganges](#Plankengang) am [→Kiel](#Kiel) und an den [→Steven](#Steven)

**[Sporholz]{#Sporholz}**  
:    [→Holzblock](#Block), der an einen [→Spant](#Spant) angelascht ist und die [→Mastspur](#Mastspur) darstellt.

**[Spring]{#Spring}**
:    Festmacherleine, die vom Vorschiff bzw. Achterschiff in Richtung Mittschiffs an der Pier festgemacht wird

**[Springperd]{#Springperd}**
:    Verbindungstau zum Arbeiten und Überwinden von kurzen Strecken in der [→Takelage](#Takelage)

*[Stag]{#Stag}*
:    stay (engl.), stag (dk.)
:    Teile des [→stehendes Gutes](#stehendesGut), der einen [→Mast](#Mast) in Längsrichtung des Schiffes stützt (im Gegensatz zu den [→Wanten](#Wanten), die in Querrichtung stützen)

*[Vorstag]{#Vorstag}*
:    [→Stag](#Stag) vom [→Fockmast](#Fockmast) zum [→Bugspriet](#Bugspriet)

*[Wasserstag]{#Wasserstag}*
:    Innerer [→Stag](#Stag) unterhalb des [→Bugspriets](#Bugspriet), oft Kette oder Rundstange

*[Stampfstag]{#Stampfstag}*
:    Unterer [→Stag](#Stag) am Bugspriet, oft Kette oder Rundstange

**[Stagsegel]{#Stagsegel}**
:    Dreieckiges Segel, wird an einem [→Stag](#Stag) gefahren und ist ein [→Schratsegel](#Schratsegel)

**[Stake]{#Stake}**
:    Stange, die für das [→Staken](#Staken) verwendet wird

**[Staken]{#Staken}**
:    Antriebstechnik, bei der das Boot mit einer Stange, [→Stake](#Stag) genannt, vom Seegrund abgestoßen wird

**[Stehendes Gut]{#StehendesGut}**
:    Das gesamte unbewegliche [→Tauwerk](#Tauwerk) an Bord eines Schiffes

**[Stellage, Stelling]{#Stellage}**
:    Brett mit 2 Querhölzern, das mit Tampen außenbords gehängt wird, um Arbeiten auszuführen

**[Stenge]{#Stenge}**
:    Angesetzte Mastverlängerung

**[Stengetopp]{#Stengetopp}**
:    Oberes Ende einer [→Stenge](#Stenge)

**[Stengewanten]{#Stengewanten}**
:    [→Wanten](#Wanten), welche von einer [→Saling](#Saling) zum [→Stengetopp](#Stengetopp) reichen

**[Steuerbord]{#Steuerbord}**  
:    starbord (engl.), styrbord (dk.) 
:    In Fahrtrichtung rechte Schiffsseite

**[Steven]{#Steven}**  
:    stem (engl.), stævn (dk.)
:    [→Balken](#Balken), der in Verlängerung des [→Kiels](#Kiel) das fordere Ende des Schiffes darstellt; für das Mittelalter werden die Typen R (rabetted), V, und W (winged) unterschieden

*[Vorsteven]{#Vorsteven}*
:    stem, fore stem (engl.), forstævn (dk.)
:    Vorderer [→Steven](#Steven)

*[Achtersteven]{#Achtersteven}*
:    sternpost, after stem (engl.), agterstævn (dk.) 
:    Hinterer [→Steven](#Steven)

*[Doppelsteven]{#Doppelsteven}*  
:    [→Stevenblock](#Steven) mit zwei aufgebogenen und übereinanderliegenden Verlängerungen, belegt durch Hjortspring und Felsbilder; in Nordeuropa in der Bronzezeit und der fühen Eisenzeit üblich

*[Blocksteven]{#Blocksteven}*  
:    [→Steven](#Steven), der nicht als Holzbalken, sondern als Holzblock ausgebildet ist. 

*[Stevenplanke]{#Stevenplanke}*  
:    [→Steven](#Steven), der nicht als Holzbalken, sondern als Holzplanke ausgebildet ist. 

*[Rampensteven]{#Rampensteven}* 
:    [→Steven](#Steven), der nicht als Holbalken, sondern als rampenartig angeordnete Planken in Verlängerung des Bodens ausgebildet ist. 

*[Flügelsteven]{#Fluegelsteven}*  
:    winged stem (engl.), fløjstævn (dk.)
:    [→Steven](#Steven) mit [→Stevenflügeln](#Stevenflügel)

*[Treppensteven]{#Treppensteven}*  
:    stepped stem (engl.), trappestævn (dk.)
:    [→Flügelsteven](#Fluegelsteven) mit individuellen Stufen für jedes Plankenende

**[Stevenflügel]{#Stevenflügel}**
:    stem-wing (engl.), stævnfløj (dk.)
:    Seitliche Erweiterung des [→Stevens](#Steven) zur Ausbildung einer [→Lannung](#Lannung)

**[Stoß]{#Stoß}**
:    butt (engl.), stød (dk.)
:    das quer geschnittene Ende einer [→Planke](#Planke) oder eines [→Balken](#Balken)s

**[Straklatten]{#Straklatten}**
:    battens, ribannds, fairing battens, spline (engl.), cent (dk.)
:    flexible Leiste um den Verlauf der Bordwand zu interpolieren

**[Strecktau]{#Strecktau}**
:    Über [→Deck](#Deck) gespanntes [→Tau](#Tauwerk) zum Festhalten

**[Stringer, Weger]{#Stringer}**  
:    stringer (engl.), forstærkning, stringer (dk.)       
:    Leisten bzw. Balken zur Längsversteifung, die innen (oder im Fall der Außenweger auch außen) auf der Bordwand angebracht sind

**[stritschen]{#stritschen}**
:    Einen durchgesetzten [→Tampen](#Tampen) nochmals durchholen

**[Stütze]{#Stütze}**
:    stanchion (engl.), støtte (dk.)
:    senkrechter Pfosten, der einen Querbalken[→Querbalken](#Querbalken) stützt

*[Süll]{#Süll}**
:    Erhöhte Türschwelle, soll das Eindringen von Wasser verhindern


## T

[<Inhalt](#inhalt)

**[Takelage, Rigg]{#Takelage}**
:    rig, rigging (engl.), rigning, tovværk, stående of løbende gods (dk.)
:    Gesamtheit aller Teile der Segeleinrichtung, Sammelbegriff für [→Masten](#Mast), [→Bäume](#Baum), [→Rahen](#Rah), [→Stengen](#Stenge), Gaffeln[→Gaffeln](#Gaffel) und das dazugehörende [→Laufende](#LaufendesGut) und [→Stehende Gut](#StehendesGut)

**[Takling]{#Takling}**
:    wird auf das Ende eines [→Tauwerkstücks](#Tauwerk) aufgebunden, damit es nicht aufdrillen kann

**[Talje]{#Talje}**
:    [→Tauwerk](#Tauwerk), das zur Kräfteersparnis durch [→Blöcke](#Block) läuft

**[Tampen]{#Tampen}**
:    Seile und [→Taue](#Tauwerk)

**[Tauwerk]{#Tauwerk}**  
:    geschlagenes oder geflochtenes Seil. 
:    Fasern/Fäden werden zu Garne (Kabelgarne) gesponnen
:    2-18 Garne = Leine (Litzen, Duchten) 
:    18-50 Garne = Kardeel 
:    3-5 Kardeele = Trosse
:    x Trossen = Kabel/Kabeltau
<!-- :    Meyers Großes Konversations-Lexikon, Band 19. Leipzig 1909, S. 359: "Tauwerk wird vom Reepschläger aus Hanf oder Manilahanf, mitunter aus Baumbast (Basttau) oder Chinesischem Gras (Grastau) hergestellt. Man spinnt den Hanf zunächst in Garne (Kabelgarne), die geteert und in der Anzahl von 2–18 zu Leinen (Litzen, Duchten) oder zu 18–50 zu einem Kardeel zusammengedreht werden. 3–5 Kardeele geben eine Trosse, aus mehreren Trossen bildet man ein Kabel oder Kabeltau. Trossen und Kabel benennt man nach ihrem Umfang in Zentimetern (3–50 cm) und nach ihrer Anfertigung: drei-, vier- oder fünfschäftig; rechts oder links geschlagen (gedreht; Kabelschlag). Laufendes Gut ist dreischäftig rechts geschlagen, stehendes vierschäftig links geschlagen, während die Kardeele, aus denen letzteres besteht, ebenfalls rechts geschlagen sind. Bei Drahttauwerk treten Eisendrähte an Stelle der Garne (s. Drahtseile)."
 --> 

**[Tau]{#Tau}**  
:    rope (engl.)
:    aus 3 Litzen geschlagen: hawser, hawser-laid 
:    aus 4 Litzen geschlagene: shroud-laid  
:    starkes Tau, aus 3 Trossen geschlagen: cable, cable-laid   

**[Teer]{#Teer}**  
:    tar (engl.)
:    Abdichtungs- und Konservierungsmasse

**[Tiefgang]{#Tiefgang}**  
:    draft, draught (engl.), dybgang (dk.)       
:    Vertikale Distanz von der tiefsten Stelle des Schiffs bis zur Ebene der Wasseroberfläche

**[Tonnenrack]{#Tonnenrack}**
:    Heißbare Halterung von [→Rahen](#Rah) am [→Mast](#Mast)

**[Topp]{#Topp}**
:    Äußerstes Ende von [→Masten](#Mast) und [→Stengen](#Stenge)

**[Toppnanten, Dumper]{#Toppnanten}**
:    Seile für das waagerechte Einstellen der [→Rahen](#Rah)

**[Toppwanten]{#Toppwanten}**
:    Kurze [→Wanten](#Wanten) oberhalb einer [→Saling](#Saling)

**[Totholz]{#Totholz}**
:    deadwood (engl.), dødtræ (dk.)
:    [→Balken](#Balken), der über dem [→Kielschwein](#Kielschwein) zur Befestigung an den  [→Spanten](#Spant) vorgesehen ist

**[Treideln]{#Treideln}**
:    towing (engl.)
:    Antriegstechnik, ziehen des Fahrzeuges mit einer Leine von Land

**[Trireme, Triere]{#Trireme}**
:    trireme (engl.)
:    Schiffstyp, antikes [→Kriegsschiff](#Kriegsschiff) mit drei übereinander liegenden Ruderreihen, Variante einer [→Galeere](#Galeere)

**[Trosse]{#Trosse}**  
:    hawser, hawser-laid     
:    Schweres, aus 3 Litzen geschlagenes [→Tau](#Tauwerk)

## U

[<Inhalt](#inhalt)

**[U-Boot]{#U-Boot}**
:    Schiffstyp für die Unterwasserfahrt

**[Übergangsplanke]{#Uebergangsplanke}**  
:    transition plank (engl.)
:    [→Planke](#Planke), of mit L-förmigem Querschnitt, die sich an der [→Kimm](#Kimm) befindet und  den Schiffsboden mit der Bordwand verbindet

**Unterliek**  
:    siehe [→Liek](#Unterliek)

## V

[<Inhalt](#inhalt)

**[vor]{#vor}**
:    before (engl.), foran (dk.)
:    Indiziert einen Punkt im Schiff, der in Fahrtrichtung weiter vorne liegt als ein Referenzpunkt.

**[voraus]{#voraus}**
:    ahead (engl.), forude (dk.)
:    Indiziert einen Punkt in Fahrrichtung vor dem Schiff

**[Vordeck]{#Vordeck}**
:    fore deck (engl.), fordæk (dk.)
:    kleines [→Deck](#Deck) im Bugbereich

**Vorliek**  
:    siehe [→Liek](#Vorliek)

**[Vorreiber]{#Vorreiber}**
:    Kräftiger Verschlußhebel an Schotten und Schiffstüren

**[Vorschiff]{#Vorschiff}**
:    forebody (engl.), forskib (dk.)
:    Der vordere Teil des Schiffes

**[Vorsegel, Vorgeschirr]{#Vorsegel}**
:    [→Stagsegel](#Stagsegel) an den [→Stagen](#Stag) des vorderen [→Mastes](#Mast)


## W

[<Inhalt](#inhalt)

**[Want, Wanten]{#Wanten}**  
:    shroud (engl.), vant (dk.)
:    Abstagungen der Masten nach den Seiten und gleichzeitig nach hinten

*[Unterwanten]{#Unterwanten}*
:    [→Wanten](#Wanten), die nur die Untermasten halten

**[Wantspanner, Wantnadel]{#Wantspanner}**
:    shroud-pin (engl.), vantnål (dk.)
:    Vorrichtung zum Spannen der [→Wanten](#Wanten)

**[Wassergang]{#Wassergang}**
:    Schmaler Streifen vor der Innenseite der Bordwand zum Ableiten von Wasser

**[Wasserlinienriss]{#Wasserlinienriss}**
:    half-breadth plan (engl.), vandlinieplan (dk.)
:    Graphische Liniendarstellung der Schnittflächen horizontaler Ebenen mit der Außenoberfläche des Schiffsrumpfes

**[Wasserpforte]{#Wasserpforte}**
:    Verschließbare Öffnung im Schanzkleid zum schnellen Abfluß des Wassers an Deck

**[Wattierung, Einlage]{#Wattierung}**  
:    wadding (engl.)     
:    Diverse Füllmaterialien

**[Webleinen]{#Webleinen}**
:    Zwischen den [→Wanten](#Wanten) angebrachte [→Leinen](#Leinen) aus [→Tauwerk](#Tauwerk), die als Sprossen zum Besteigen der [→Masten](#Mast) dienen

**[Webleinenstek]{#Webleinenstek}**
:    Knoten zur Kreuzung der [→Webeleinen](#Webleinen) mit den [→Wanten](#Wanten)

**[Wegerung]{#Wegerung}**
:    ceiling (engl.), garnering (dk.)
:    Innenbeplankung

**[Wende]{#Wende}**
:    Schiff mit dem [→Bug](#Bug) durch den Wind drehen

## X

[<Inhalt](#inhalt)

## Y

[<Inhalt](#inhalt)

## Z

[<Inhalt](#inhalt)

**[Zapfenverbindung]{#Zapfenverbindung}**  
:    mortise-and-tenon (engl.)       
:    [→Nut-und-Feder-Verbindung](#NutundFeder)

**[Zeising]{#Zeising}**
:    Dünnes [→Tauwerk](#Tauwerk) zum Befestigen eingeholter [→Segel](#Segel)

**[Zierprofil]{#Zierprofil}**
:    moulding (engl.), profil, profilering, pynteprofil (dk.)
:    längs laufende Zierrillen im Holz

**[zurren]{#zurren}**
:    Etwas seefest anbinden

**[Zwischenstag]{#Zwischenstag}**
:    [→Stag](#Stag) bei einer [→Schonerbrigg](#Schonerbrigg) vom [→Großmast](#Grossmast) zum [→Fockmast](#Fockmast)

**[Zwischenstagsegel]{#Zwischenstagsegel}**
:    [→Segel](#Segel), die an den [→Zwischenstagen](#Zwischenstag) gefahren werden

# Quellen

[<Inhalt](#inhalt)

- Crumlin-Pedersen, O. (1997). Viking-Age ships and shipbuilding in Hedeby/Haithabu and Schleswig. Roskilde: Viking Ship Museum.
- Englert, A. (2015). Large cargo ships in Danish waters 1000-1250: Evidence of specialised merchant seafaring prior to the Hanseatic period. Roskilde: The Viking Ship Museum, Roskilde.
- Lemée, Christian P. P. (2006). The Renaissance Shipwrecks from Christianshavn. Roskilde: The Viking Ship Museum, Roskilde.
- [Oxford Handbook](https://www.oxfordhandbooks.com/view/10.1093/oxfordhb/9780199336005.001.0001/oxfordhb-9780199336005-e-48
)
- [Seemaennische Begriffe Greif](https://sssgreif.de/index.php/das-schiff/seemaennische-begriffe) 
- [Holzbootbau](https://holz-boot.com/category/lexikon/) 


<!-- 
- [wikipedia](https://de.wikipedia.org/wiki/Liste_seem%C3%A4nnischer_Fachw%C3%B6rter_(A_bis_M)#D)
- [marine](https://www.ostarische-marine.de/Dienstordnung/Nautische%20Begriffe)
 --> 
